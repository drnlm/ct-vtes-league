League Rules for 2012

   1. League games take place during the regular VtES socials.
   2. Generally, the first round of a social counts towards the league, but, if all players agree beforehand, the league game(s) can be one of the later rounds. Only 1 round of league games can be played at any given social.
   3. The last league game will be is played during a social on 27th of the June. Prizes will also be distributed then.
   4. All participants, who want to play a league game, must decide what deck they want to use for the league match.
   5. Then the table assignment of the players are determined:
         1. The players which are on top of the current ranking play on the first table. This table should consists of five players when possible.
         2. For the rest of the players a random assignment to tables is done.
         3. If for some reason the current ranking is not clear, all players are assigned to table randomly.
         4. The table assignment during the first meeting of a year is always at random.
         5. The seating order on all tables is determined randomly, as well who is the starting player at any given table.
   6. League games are conducted following the the official VEKN tournament rules, unless stated otherwise.
         1. All rulebooks, rulings and card texts are in effect as soon as they are published.
         2. New cards are allowed for play in the league games the day they are published.
         3. We reserve the possibility to use special rules/formats for league games when announced so before the regular VtES meetings.
   7. The timelimit for the games are two hours. If the number of players attending the meeting 6, 7 or 11, then tables with 6 or 7 players are allowed, but with the following special rules:
         1. For a 6 player table the time is extended to 2.5 hours.
         2. For a 7 player table the time is extended to 3 hours.
         3. If there are more than 5 players at a table, all cards that refer to the number of players at the table (e.g. Conservative Agitation, Parity Shift, Slave Auction) are regarded as if there are 5 players at the table.
   8. The record for each of the players includes: number of games, number of game wins, number of victory points.
   9. From these numbers the ranking is determined in the following way:
         1. A Game Win (GW) is worth 8 Rating Points (RtP), and a Victory Point (VP) is worth 4 RtP)
         2. The ranking for each player is determined by the average number of RtP per game of the player's 6 best results (rounded to two significant digits).
         3. Players who haven't participated in the league have 0 Rtp/Game, and therefore are the end of the league's ranking.
         4. If two players tie for ranking place, the player with more games is ahead. If there's still a tie, the player with less Game Losses and (then Warnings) is ahead. If there's still a tie then, the leading player is determined randomly.
         5. If a player has accumulated three warnings for the same offense during the current year, he receives an immediate game loss for the current game.
         6. Results and the current ranking will be tracked on the "VTES in Cape Town" website.
         7. Only players who participated in at least 6 league games enter the final ranking.
  10. During the course of the league we'll collect a number of prizes for the league. At the end of the last league meeting the prizes are distributed in the following way:
         1. All players present at this meeting, can choose one of the prizes in the order of the league's final ranking.
         2. If anybody misses this meeting, and they haven't specified a favourite prize, no prize is reserved for them, but they can reclaim a prize later that hasn't been chosen yet.
