#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim:fileencoding=utf8 ai ts=4 sts=4 et sw=4
# Script to calculate VPs & GWs from seating and ousting order entries
# Copyright 2012 Neil Muller <drnlmuller+vtes@gmail.com>
# License: GPL - See LICENSE file for details

import sys


def process_file(sFileName):
    """Extract the info from the file.

       File format isn't well defined yet.
       Required sections are:
       Name Mapping:
       which is followed by a set of pairs of the form
          Shortname  Full name
       where Shortname is assumed to be 1 word

       and Game: <game details>
        which is expected to contain table sections
           Table X
        with sections
              Seating order:
                  Name (deck)

           and

              Ousting order:
                  Name
        and a section
           Results:
              Shortname X GP, X VP
        entries
    """
    def check_timeout():
        if len(aSeating) > 1:
            print 'Timeout found %s:%s' % (sGame, sTable)
            for sName in aSeating:
                dGames[sGame][sTable][sName] += 0.5
        elif len(aSeating) == 1 and len(dGames[sGame][sTable]) == 1:
            print 'Single man event found %s:%s' % (sGame, sTable)
            for sName in aSeating:
                dGames[sGame][sTable][sName] = 0.5
    # I should probably use yaml or something like that instead
    UNKNOWN, TABLE, SEATING, OUSTING, GAME = range(5)
    oState = UNKNOWN
    dGames = {}
    aSeating = []
    sGame = ''

    with open(sFileName, 'rU') as oFile:
        for iLineNo, sLine in enumerate(oFile):
            sLine = sLine.strip()
            # Identify useful sections
            if sLine.startswith('Game: '):
                check_timeout()
                aSeating = []
                oState = GAME
                sGame = sLine.split(':', 1)[1].strip()
                dGames[sGame] = {}
                sTable = ''
                continue
            elif oState == TABLE and sLine.startswith('Seating'):
                oState = SEATING
                continue
            elif oState == SEATING and sLine.startswith('Ousting'):
                oState = OUSTING
                continue
            elif (oState == GAME or oState == OUSTING) and \
                    sLine.startswith('Table'):
                check_timeout()
                aSeating = []
                oState = TABLE
                sTable = sLine.strip()
                dGames[sGame][sTable] = {}
                continue
            elif oState == OUSTING and sLine.startswith('Results'):
                oState = GAME
                continue
            if oState == SEATING:
                if not sLine.strip():
                    continue
                sName = sLine.split('(', 1)[0].strip()
                aSeating.append(sName)
                dGames[sGame][sTable][sName] = 0
            elif oState == OUSTING:
                if not sLine.strip():
                    continue  # skip blanks
                sName = sLine.strip()
                if sName not in aSeating:
                    raise RuntimeError('unrecognised ousting entry in %s'
                            ' (line %d): %s' % (sGame, iLineNo, sName))
                iPred = aSeating.index(sName) - 1
                if iPred < 0:
                    sPred = aSeating[-1]
                else:
                    sPred = aSeating[iPred]
                aSeating.remove(sName)
                dGames[sGame][sTable][sPred] += 1
                if len(aSeating) == 1:
                    # last man standing
                    dGames[sGame][sTable][sPred] += 1

    check_timeout()
    return dGames


def format_vps_gws(dGames):
    """Print results nicely. Also add GWs"""
    for sGame in sorted(dGames):
        print sGame
        for sTable in sorted(dGames[sGame]):
            print
            print sTable
            aScores = sorted(dGames[sGame][sTable].items(),
                    key=lambda x: (-x[1], x[0]))
            maxScore = max(dGames[sGame][sTable].values())
            maxCount = dGames[sGame][sTable].values().count(maxScore)
            namelen = max(len(x[0]) for x in aScores)
            for sName, fVP in aScores:
                sName = sName.ljust(namelen)
                # at least 2 VPs and more than anyone else is a GW
                if fVP == maxScore and maxScore >= 2 and maxCount == 1:
                    print '    %s  1 GW, %.1f VPs' % (sName, fVP)
                else:
                    print '    %s  0 GW, %.1f VPs' % (sName, fVP)
        print


if __name__ == "__main__":
    try:
        sResultsFile = sys.argv[1]
    except IndexError:
        print "Usage: %s <results files>" % sys.argv[0]
        print "Prints the standings table formatted for pmwiki to stdout"
        sys.exit(1)

    dGames = process_file(sResultsFile)
    format_vps_gws(dGames)
