#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim:fileencoding=utf8 ai ts=4 sts=4 et sw=4
# Script to generate the standings table for the CT league
# Copyright 2012 Neil Muller <drnlmuller+vtes@gmail.com>
# License: GPL - See LICENSE file for details

import sys
import re
import optparse

from operator import methodcaller

NOT_QUALIFIED = '#ee2222'
QUALIFIED = '#0033dd'
MIN_TABLE_SIZE = 4
NON_EVENT = '#997799'

ANSI_RED = "\033[31m"
ANSI_RESET = "\033[0m"

ORDERINGS = ['ave', 'qualified']

# Games required to qualify
REQUIRED = 6


def print_err(sMsg):
    """Colorful error message"""
    print ''.join([ANSI_RED, sMsg, ANSI_RESET])


class PlayerStats(object):
    """Utility class for player results"""

    __slots__ = ['total_vps', 'rounded_vps', 'gws', 'rps', 'ave', 'games',
            'best', 'name', 'order']

    def __init__(self, sName, sOrder):
        self.total_vps = 0.0
        self.rounded_vps = 0
        self.gws = 0
        self.rps = 0
        self.ave = 0.0
        self.games = 0
        self.best = 0
        self.name = sName
        self.order = sOrder

    def sortkey(self):
        """Sort  key for tables and email list"""
        if self.order == 'qualified':
            # We sort qualified playtes (with REQUIRED or more games
            # before those who still need to qualify, then descending
            # by ave, then by games, then by name
            return (self.games < REQUIRED, -self.ave, -self.total_vps,
                    -self.games, self.name)
        elif self.order == 'ave':
            return -self.ave, -self.total_vps, -self.games, self.name
        else:
            raise RuntimeError('Unknown ordering')

    def get_email_line(self):
        """Return a line formatted for email"""
        if self.games == 1:
            sGames = 'game'
        else:
            sGames = 'games'
        sDetails = ('%d %s, %d GWs, %.1f VPs (%d VPs for rating),'
                ' %d total RPs (best %d: %d), %.2f ave') % (
                    self.games, sGames, self.gws, self.total_vps,
                    self.rounded_vps, self.rps, REQUIRED, self.best, self.ave)
        if self.games >= REQUIRED:
            return '%s *: %s (best %d)' % (self.name, sDetails, REQUIRED)
        else:
            return '%s: %s' % (self.name, sDetails)

    def get_wiki_line(self):
        """Return a line formatted for the wiki"""
        sDetails = ' %d || %d || %.1f || %d || %d || %d || %.2f ' % (
                self.games, self.gws, self.total_vps, self.rounded_vps,
                self.rps, self.best, self.ave)
        if self.games >= REQUIRED:
            return '||%%color=%s%% %s %%%%||%s||' % (QUALIFIED, self.name,
                    sDetails)
        else:
            return '||%%color=%s%% %s %%%%||%s||' % (NOT_QUALIFIED, self.name,
                    sDetails)


def parse_options(aArgs):
    """Handle command line arguments"""

    oOptParser = optparse.OptionParser(usage="usage: %prog [options] file",
            version="%prog 0.1")
    oOptParser.add_option('-e', '--email', action='store_true',
            dest='email', default=False,
            help="Format for email (default %default)")
    oOptParser.add_option('-o', '--order', type='choice',
            dest='ordering', choices=ORDERINGS,
            default='ave',
            help="Select the output order:"
            " Choices are [%s], default %%default."
            " The ave option sorts on the average RPs,"
            " but the qualified option sorts qualified players (with %d games"
            " or more) above those who haven't qualfied (intended for showing"
            " the final positions)" % (", ".join(ORDERINGS), REQUIRED))
    return oOptParser, oOptParser.parse_args(aArgs)


def process_file(sFileName, bEmail):
    """Extract the info from the file.

       File format isn't well defined yet.
       Required sections are:
       Name Mapping:
       which is followed by a set of pairs of the form
          Shortname  Full name
       where Shortname is assumed to be 1 word

       and Game: <game details>
        which is expected to contain table sections
           Table X
        with sections
              Seating order:
                  Name (deck)

           and

              Ousting order:
                  Name
        and a section
           Results:
              Shortname X GP, X VP
        entries
    """
    # I should probably use yaml or something like that instead
    UNKNOWN, MAPPING, GAME, RESULTS = range(4)
    SCORE_RE_1 = re.compile(
            '(?P<gw>[0-9]+)[ ]*gw[s,]*[ ]+(?P<vp>[0-9]+(\.[0-9])?)[ ]*vp')
    SCORE_RE_2 = re.compile(
            '(?P<vp>[0-9]+(\.[0-9])?)[ ]*vp[s,]*[ ]+(?P<gw>[0-9]+)[ ]*gw')
    oState = UNKNOWN
    dNames = {}
    dGames = {}
    dScores = {}
    with open(sFileName, 'rU') as oFile:
        for iLine, sLine in enumerate(oFile):
            sLine = sLine.strip()
            # Identify useful sections
            if sLine == 'Name Mapping:':
                oState = MAPPING
                continue
            elif sLine.startswith('Game: '):
                oState = GAME
                sGame = sLine.split(':', 1)[1].strip()
                dGames[sGame] = {}
                continue
            elif oState == GAME and sLine == 'Results:':
                oState = RESULTS
                continue
            elif oState == RESULTS and sLine.startswith('Table'):
                oState = GAME
                continue
            elif sLine.startswith('Results') or sLine.startswith('Game'):
                # Some very basic error checking
                print_err('Possibly incorrect marker line seen at %d (%s)' % (
                        iLine, sLine))

            if oState == MAPPING and sLine:
                sShortName, sFullName = sLine.split(' ', 1)
                dNames[sShortName.lower()] = sFullName.strip()
            elif oState == RESULTS and sLine:
                sResult = sLine.lower()
                if not 'gw' in sResult or not 'vp' in sResult:
                    print_err('Results line missing vp or gw value: %s'
                            % sLine)
                    continue
                sShortName, sScore = sResult.split(' ', 1)
                if sShortName not in dNames:
                    print_err('Unrecognised name: %s from %s' % (sShortName,
                            sLine))
                    sFullName = sShortName
                else:
                    if bEmail:
                        # Use shortnames if it's unambigious
                        # We use the Name_X convention for ambigious names
                        if '_' in sShortName:
                            sFullName = dNames[sShortName]
                        else:
                            sFullName = sShortName.capitalize()
                    else:
                        sFullName = dNames[sShortName]
                oMatch = SCORE_RE_1.search(sScore)
                if not oMatch:
                    oMatch = SCORE_RE_2.search(sScore)
                if not oMatch:
                    print_err('Unable to identify scores from %s' % sLine)
                else:
                    try:
                        fVPs = float(oMatch.groupdict()['vp'])
                        iGWs = int(oMatch.groupdict()['gw'])
                        iRPs = 8 * iGWs + 4 * int(fVPs)
                    except ValueError:
                        print_err('Unable to identify scores from %s' % sLine)
                        continue
                    dGames[sGame][sFullName] = (iGWs, fVPs, iRPs)
                    dScores.setdefault(sFullName, []).append(iRPs)
    # We now process the results to get the correct scores
    dResults = {}
    for sPlayer, aScores in dScores.iteritems():
        aScores.sort(reverse=True)
        if len(aScores) > REQUIRED:
            fTot = sum(aScores[:REQUIRED]) / float(REQUIRED)
            iBest = sum(aScores[:REQUIRED])
        else:
            fTot = sum(aScores) / float(len(aScores))
            iBest = sum(aScores)
        dResults[sPlayer] = (fTot, iBest)
    return dGames, dResults


def make_table_dict(dGames, dResulsts, sOrder):
    """Generate a dict from the processed results"""
    dTableEntries = {}
    for sGame in sorted(dGames):
        for sPlayer, oData in dGames[sGame].iteritems():
            dTableEntries.setdefault(sPlayer, PlayerStats(sPlayer, sOrder))
            dTableEntries[sPlayer].gws += oData[0]
            dTableEntries[sPlayer].total_vps += oData[1]
            dTableEntries[sPlayer].rounded_vps += int(oData[1])
            dTableEntries[sPlayer].rps += oData[2]
            dTableEntries[sPlayer].games += 1
    iQual = 0
    for sPlayer in dTableEntries:
        dTableEntries[sPlayer].ave = dResults[sPlayer][0]
        dTableEntries[sPlayer].best = dResults[sPlayer][1]
        if dTableEntries[sPlayer].games >= REQUIRED:
            iQual += 1
    aTableEntries = dTableEntries.values()
    aTableEntries.sort(key=methodcaller('sortkey'))
    return aTableEntries, iQual


def format_results(dGames, aTableEntries, iQual):
    print '!!League games run'
    iNonEvents = 0
    for sGame in sorted(dGames):
        if len(dGames[sGame]) < MIN_TABLE_SIZE:
            print '* %%color=%s%% %s (Non-Event) %%%%' % (NON_EVENT, sGame)
            iNonEvents += 1
        elif sGame.endswith('(non-event)'):
            print '* %%color=%s%% %s (Non-Event) %%%%' % (NON_EVENT,
                    sGame.replace('(non-event)', '').rstrip())
            iNonEvents += 1
        else:
            print '* %s' % sGame
    print
    if iNonEvents > 0:
        print 'Total games: %d (%d non-events)' % (len(dGames), iNonEvents)
    else:
        print 'Total games: %d' % len(dGames)
    print
    print '!!Standings'
    print '||border=1'
    print ("|| '''Player'''        || '''Games''' || '''GWs''' ||"
            " '''VPs''' || '''VPs for rating''' || '''Total RPs''' ||"
            " '''RPs (best %d games)''' || '''Average RPs (best %d)''' ||"
            % (REQUIRED, REQUIRED))
    for oStats in aTableEntries:
        print oStats.get_wiki_line()
    print
    print '%d (out of %d) players qualified' % (iQual, len(aTableEntries))
    print
    # Add the color code table
    print 'Colour codes'
    print '||border=1'
    print ('||%%color=%s%% less than %d games %%%%'
           '||Not yet qualified for final ranking||' %
           (NOT_QUALIFIED, REQUIRED))
    print ('||%%color=%s%% %d or more games %%%%'
            '||Qualified for the final ranking||' %
            (QUALIFIED, REQUIRED))


def format_email(dGames, aTableEntries, iQual):
    iNonEvents = 0
    for sGame in dGames:
        if len(dGames[sGame]) < MIN_TABLE_SIZE:
            iNonEvents += 1
        elif sGame.endswith('(non-event)'):
            iNonEvents += 1
    if iNonEvents > 0:
        print 'League Standings after %d games (%d non-events)' % (
                len(dGames), iNonEvents)
    else:
        print 'League Standings after %d games' % len(dGames)
    print
    for oStats in aTableEntries:
        print oStats.get_email_line()
    print
    print '%d (out of %d) players qualified' % (iQual, len(aTableEntries))
    print
    print '* Qualified for final league standings (%d games)' % REQUIRED


if __name__ == "__main__":
    bEmail = False
    oOptParser, (oOpts, aArgs) = parse_options(sys.argv)
    if len(aArgs) != 2:
        oOptParser.print_help()
        sys.exit(1)
    sResultsFile = aArgs[1]
    try:
        dGames, dResults = process_file(sResultsFile, oOpts.email)
    except IOError, e:
        print_err('Unable to read %s' % sResultsFile)
        print_err('Error was: %s' % e)
        sys.exit(1)
    aTableEntries, iQual = make_table_dict(dGames, dResults, oOpts.ordering)
    if oOpts.email:
        format_email(dGames, aTableEntries, iQual)
    else:
        format_results(dGames, aTableEntries, iQual)
